# Prototyping Boards

This repository contains PCB layouts for a few 10x10cm prototyping boards

* EarlsProto is a straightforward protoboard
* EarlsProto2 is similar but with more pads along the top edge for LCDs 
and possible other offboard attachments
* PICproto includes pads for a PICkit programming connector and a 
micro-USB for power
